﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Filemanager
{
    public class Logline
    {
        public string fromuser, touser, note;
        public int state;
        public DateTime date;

        public string State
        {
            get
            {
                switch (state)
                {
                    case 1:
                        return "Отправлен на подтверждение";
                    case 2:
                        return "Отправлен на доработку";
                    case 3:
                        return "Завершен";
                    default:
                        return "В работе";
                }
            }
            set
            {
                switch (value)
                {
                    case "Отправлен на подтверждение":
                        this.state = 1;
                        break;
                    case "Отправлен на доработку":
                        this.state = 2;
                        break;
                    case "Завершен":
                        this.state = 3;
                        break;
                    default:
                        this.state = 0;
                        break;
                }
            }
        }
        //reading logs
        public Logline(string str)
        {
            this.state = int.Parse(str[0].ToString());
            int i = 5;
            while (str.Substring(i,4)!=" -- ")
            {
                this.fromuser += str[i];
                i++;
            }
            i += 4;
            while (str.Substring(i,4)!=" -- ")
            {
                this.touser += str[i];
                i++;
            }
            i += 4;
            this.date = Convert.ToDateTime(str.Substring(i, 19));
            i += 22;
            while (i!=str.Length)
            {
                this.note += str[i];
                i++;
            }
        }
    }
}