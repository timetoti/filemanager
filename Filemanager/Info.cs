﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Filemanager
{
    public partial class Info : Form
    {
        MiddleForm md;
        public Info(MiddleForm md)
        {
            InitializeComponent();

            this.md = md;
            
            richTextBox1.LoadFile(@"C:\Folderfortricks\Info.rtf");
        }

        private void Info_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.md.info = null;
        }
    }
}
