﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Filemanager
{
    public partial class Usertemplates : Form
    {
        string path;
        bool create;
        string name;
        int templnum;
        Word.Application WordApp;
        Word.Document WordDoc;
        object miss = Type.Missing;
        object newTemplate = false;
        object documentType = Word.WdNewDocumentType.wdNewBlankDocument;
        List<string> str = new List<string>();

        public Usertemplates(bool create, string path = "", int templnum = 0, string name = "")
        {
            InitializeComponent();
            this.create = create;
            if (create==true)
            {
                this.Text = "Создание пользовательского шаблона";
                this.templnum = System.IO.Directory.GetFiles(@"C:\Folderfortricks", "usertemplate*.docx").Length + 1;
                object visible = true;
                WordApp = new Word.Application { Visible = true };
                WordDoc = WordApp.Documents.Add(ref miss, ref newTemplate, ref documentType, ref visible);
                WordDoc.Activate();
            }
            else
            {
                this.templnum = templnum;
                this.path = path;
                this.name = name;
                this.Text += ' ' + name;
                Controlfunc.Makeitvisible(dataGridView1, Endwork);
                Controlfunc.Makeitinvisible(Endcreate, namebox, namelabel);
                WordApp = new Word.Application { Visible = false };
                object readOnly = false;
                object fp = @"C:\Folderfortricks\usertemplate" + templnum.ToString() + name + ".docx";
                WordDoc = WordApp.Documents.Open(ref fp, ref miss, ref readOnly, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);
                WordDoc.ActiveWindow.Selection.WholeStory();
                WordDoc.ActiveWindow.Selection.Copy();
                IDataObject data = Clipboard.GetDataObject();
                string alldoc = data.GetData(DataFormats.Text).ToString();
                int i = 0;
                while (i < alldoc.Length)
                {
                    if (alldoc[i] == '<')
                    {
                        string s = "";
                        i++;
                        while (alldoc[i]!='>')
                        {
                            s += alldoc[i];
                            i++;
                        }
                        str.Add(s);
                    }
                    i++;
                }

                foreach(string s in str)
                {
                    dataGridView1.Rows.Add(s, "");
                }
            }
        }

        private void Endcreate_Click(object sender, EventArgs e)
        {
            if (namebox.Text != "")
            {
                if (!namebox.Text.Contains('0') && !namebox.Text.Contains('1') && !namebox.Text.Contains('2') && !namebox.Text.Contains('3') && !namebox.Text.Contains('4') && !namebox.Text.Contains('5') && !namebox.Text.Contains('6') && !namebox.Text.Contains('7') && !namebox.Text.Contains('8') && !namebox.Text.Contains('9'))
                {
                    name = namebox.Text;
                    path = @"C:\Folderfortricks\usertemplate" + templnum.ToString() + name + ".docx";
                    WordDoc.SaveAs2(FileName: path);
                    WordDoc.Close();
                    WordApp.Quit();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Нельзя использовать цифры в названии шаблона");
                }
            }
            else
            {
                MessageBox.Show("Введите имя шаблона!");
            }
        }

        private void Endwork_Click(object sender, EventArgs e)
        {
            for (int j = 0; j < dataGridView1.Rows.Count; j++)
            {
                Word.Find find = WordApp.Selection.Find;
                find.Text = '<'+str[j]+'>';
                find.Replacement.Text = dataGridView1.Rows[j].Cells[1].Value.ToString();
                Object wrap = Word.WdFindWrap.wdFindContinue;
                Object replace = Word.WdReplace.wdReplaceAll;
                find.Execute(FindText: Type.Missing,
                    MatchCase: false,
                    MatchWholeWord: false,
                    MatchWildcards: false,
                    MatchSoundsLike: miss,
                    MatchAllWordForms: false,
                    Forward: true,
                    Wrap: wrap,
                    Format: false,
                    ReplaceWith: miss, Replace: replace);
            }
            WordDoc.SaveAs2(FileName: path);
            WordDoc.Close();
            WordApp.Quit();
            this.Close();
        }
    }
}