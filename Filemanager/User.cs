﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Filemanager
{
    public class User
    {
        public User(string name, string pass)
        {
            this.name = name;
            this.pass = pass;
        }

        public User(string name, int accesslevel, string pass)
        {
            this.name = name;
            this.accesslevel = accesslevel;
            this.pass = pass;
        }

        public string name;
        public int accesslevel;
        public string pass;

        public string Accesslevel
        {
            get
            {
                switch (this.accesslevel)
                {
                    case 0:
                        return "Руководство";
                    case 1:
                        return "Менеджер";
                    default:
                        return "Работник";
                }
            }
            set
            {
                switch(value)
                {
                    case "Руководство":
                        this.accesslevel = 0;
                        break;
                    case "Менеджер":
                        this.accesslevel = 1;
                        break;
                    default:
                        this.accesslevel = 2;
                        break;
                }
            }
        }
    }
}