﻿namespace Filemanager
{
    partial class Usertemplates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.templ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Endcreate = new System.Windows.Forms.Button();
            this.Endwork = new System.Windows.Forms.Button();
            this.namelabel = new System.Windows.Forms.Label();
            this.namebox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ColumnHeadersVisible = false;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.templ,
            this.value});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(603, 293);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.Visible = false;
            // 
            // templ
            // 
            this.templ.HeaderText = "Column1";
            this.templ.Name = "templ";
            this.templ.Width = 300;
            // 
            // value
            // 
            this.value.HeaderText = "Column1";
            this.value.Name = "value";
            this.value.Width = 300;
            // 
            // Endcreate
            // 
            this.Endcreate.Location = new System.Drawing.Point(12, 315);
            this.Endcreate.Name = "Endcreate";
            this.Endcreate.Size = new System.Drawing.Size(197, 23);
            this.Endcreate.TabIndex = 1;
            this.Endcreate.Text = "Закончить создание шаблона";
            this.Endcreate.UseVisualStyleBackColor = true;
            this.Endcreate.Click += new System.EventHandler(this.Endcreate_Click);
            // 
            // Endwork
            // 
            this.Endwork.Location = new System.Drawing.Point(383, 315);
            this.Endwork.Name = "Endwork";
            this.Endwork.Size = new System.Drawing.Size(232, 23);
            this.Endwork.TabIndex = 2;
            this.Endwork.Text = "Перейти к выбору назначения документа";
            this.Endwork.UseVisualStyleBackColor = false;
            this.Endwork.Visible = false;
            this.Endwork.Click += new System.EventHandler(this.Endwork_Click);
            // 
            // namelabel
            // 
            this.namelabel.AutoSize = true;
            this.namelabel.Location = new System.Drawing.Point(215, 320);
            this.namelabel.Name = "namelabel";
            this.namelabel.Size = new System.Drawing.Size(147, 13);
            this.namelabel.TabIndex = 3;
            this.namelabel.Text = "Введите название шаблона";
            // 
            // namebox
            // 
            this.namebox.Location = new System.Drawing.Point(368, 318);
            this.namebox.Name = "namebox";
            this.namebox.Size = new System.Drawing.Size(250, 20);
            this.namebox.TabIndex = 4;
            // 
            // Usertemplates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 347);
            this.Controls.Add(this.namebox);
            this.Controls.Add(this.namelabel);
            this.Controls.Add(this.Endwork);
            this.Controls.Add(this.Endcreate);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Usertemplates";
            this.Text = "Пользовательский шаблон";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn templ;
        private System.Windows.Forms.DataGridViewTextBoxColumn value;
        private System.Windows.Forms.Button Endcreate;
        private System.Windows.Forms.Button Endwork;
        private System.Windows.Forms.Label namelabel;
        private System.Windows.Forms.TextBox namebox;
    }
}