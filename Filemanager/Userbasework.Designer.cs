﻿namespace Filemanager
{
    partial class Userbasework
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Confirmchanges = new System.Windows.Forms.Button();
            this.Attentionlabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Uname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uaccess = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Upass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Uname,
            this.Uaccess,
            this.Upass});
            this.dataGridView.Location = new System.Drawing.Point(25, 20);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.Size = new System.Drawing.Size(355, 199);
            this.dataGridView.TabIndex = 0;
            // 
            // Confirmchanges
            // 
            this.Confirmchanges.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Confirmchanges.Location = new System.Drawing.Point(280, 240);
            this.Confirmchanges.Name = "Confirmchanges";
            this.Confirmchanges.Size = new System.Drawing.Size(100, 44);
            this.Confirmchanges.TabIndex = 2;
            this.Confirmchanges.Text = "Подтвердить изменения ";
            this.Confirmchanges.UseVisualStyleBackColor = false;
            this.Confirmchanges.Click += new System.EventHandler(this.ConfirmChanges_Click);
            // 
            // Attentionlabel
            // 
            this.Attentionlabel.AutoSize = true;
            this.Attentionlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Attentionlabel.Location = new System.Drawing.Point(12, 287);
            this.Attentionlabel.Name = "Attentionlabel";
            this.Attentionlabel.Size = new System.Drawing.Size(374, 13);
            this.Attentionlabel.TabIndex = 3;
            this.Attentionlabel.Text = "Внимание! Для сохранения изменений нажмите кнопку подтверждения";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 222);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(389, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Для удаления пользователя просто удалите его имя или пароль в таблице";
            // 
            // Uname
            // 
            this.Uname.HeaderText = "Имя пользователя";
            this.Uname.Name = "Uname";
            // 
            // Uaccess
            // 
            this.Uaccess.HeaderText = "Уровень доступа";
            this.Uaccess.Items.AddRange(new object[] {
            "Руководство",
            "Менеджер",
            "Работник"});
            this.Uaccess.Name = "Uaccess";
            this.Uaccess.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Uaccess.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Uaccess.Width = 150;
            // 
            // Upass
            // 
            this.Upass.HeaderText = "Пароль";
            this.Upass.Name = "Upass";
            // 
            // Userbasework
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 311);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Attentionlabel);
            this.Controls.Add(this.Confirmchanges);
            this.Controls.Add(this.dataGridView);
            this.Name = "Userbasework";
            this.Text = "Редактирование базы пользователей";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button Confirmchanges;
        private System.Windows.Forms.Label Attentionlabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Uname;
        private System.Windows.Forms.DataGridViewComboBoxColumn Uaccess;
        private System.Windows.Forms.DataGridViewTextBoxColumn Upass;
    }
}